package ru.bakhtiyarov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bakhtiyarov.tm.entity.User;

import java.util.List;

public interface IUserRepository {

    List<User> findAll();

    void addAll(@NotNull List<User> user);

    void clear();

    @Nullable
    User findById(@NotNull String id);

    @Nullable
    User findByLogin(@NotNull String login);

    @Nullable
    User add(@NotNull User user);

    @Nullable
    User removeByLogin(@NotNull String login);

    @Nullable
    User removeById(@NotNull String id);

    @NotNull
    User removeUser(@NotNull User user);

}
