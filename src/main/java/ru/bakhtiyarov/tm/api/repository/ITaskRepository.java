package ru.bakhtiyarov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bakhtiyarov.tm.entity.Task;

import java.util.List;

public interface ITaskRepository {

    void add(@NotNull String userId, @NotNull Task project);

    void addAll(@NotNull List<Task> tasks);

    void remove(@NotNull String userId, @NotNull Task project);

    @NotNull
    List<Task> findAll(@NotNull String userId);

    @NotNull
    List<Task> findAll();

    void clear(@NotNull String userId);

    void clear();

    @Nullable
    Task findOneById(@NotNull String userId,@NotNull String id);

    @Nullable
    Task findOneByIndex(@NotNull String userId,@NotNull Integer index);

    @Nullable
    Task findOneByName(@NotNull String userId,@NotNull String name);

    @Nullable
    Task removeOneByIndex(@NotNull String userId,@NotNull Integer index);

    @Nullable
    Task removeOneById(@NotNull String userId,@NotNull String id);

    @Nullable
    Task removeOneByName(@NotNull String userId,@NotNull String name);

}
