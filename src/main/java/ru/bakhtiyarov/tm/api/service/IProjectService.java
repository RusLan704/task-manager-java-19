package ru.bakhtiyarov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bakhtiyarov.tm.entity.Project;

import java.util.List;

public interface IProjectService {

    void create(@Nullable String userId, @Nullable String name);

    void create(@Nullable String userId, @Nullable String name, @Nullable String description);

    void add(@Nullable String userId, @Nullable Project task);

    void addAll(@Nullable List<Project> projects);

    void remove(@Nullable String userId, @Nullable Project task);

    @NotNull
    List<Project> findAll(@Nullable String userId);

    @NotNull
    List<Project> findAll();

    void clear(@Nullable String userId);

    void clear();

    @Nullable
    Project findOneById(@Nullable String userId, @Nullable String id);

    @Nullable
    Project findOneByIndex(@Nullable String userId, @Nullable Integer index);

    @Nullable
    Project findOneByName(@Nullable String userId, @Nullable String name);

    @Nullable
    Project removeOneByIndex(@Nullable String userId, @Nullable Integer index);

    @Nullable
    Project removeOneById(@Nullable String userId, @Nullable String id);

    @Nullable
    Project removeOneByName(@Nullable String userId, @Nullable String name);

    @Nullable
    Project updateProjectById(@Nullable String userId, @Nullable String id, @Nullable String name, @Nullable String description);

    @Nullable
    Project updateProjectByIndex(@Nullable String userId, @Nullable Integer index, @Nullable String name, @Nullable String description);

}
