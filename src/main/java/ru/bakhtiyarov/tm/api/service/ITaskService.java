package ru.bakhtiyarov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bakhtiyarov.tm.entity.Task;

import java.util.Collection;
import java.util.List;

public interface ITaskService {

    void create(@Nullable String userId,@Nullable String name);

    void create(@Nullable String userId,@Nullable String name,@Nullable String description);

    void add(@Nullable String userId,@Nullable Task task);

    void addAll(@Nullable List<Task> tasks);

    void remove(@Nullable String userId,@Nullable Task task);

    @NotNull
    List<Task> findAll(@Nullable String userId);

    @NotNull
    List<Task> findAll();

    void clear(@Nullable String userId);

    void clear();

    @Nullable
    Task findOneById(@Nullable String userId,@Nullable String id);

    @Nullable
    Task findOneByIndex(@Nullable String userId,@Nullable Integer index);

    @Nullable
    Task findOneByName(@Nullable String userId,@Nullable String name);

    @Nullable
    Task removeOneByIndex(@Nullable String userId,@Nullable Integer index);

    @Nullable
    Task removeOneById(@Nullable String userId,@Nullable String id);

    @Nullable
    Task removeOneByName(@Nullable String userId,@Nullable String name);

    @Nullable
    Task updateTaskById(@Nullable String userId,@Nullable String id,@Nullable String name,@Nullable String description);

    @Nullable
    Task updateTaskByIndex(@Nullable String userId,@Nullable Integer index,@Nullable String name,@Nullable String description);

}
