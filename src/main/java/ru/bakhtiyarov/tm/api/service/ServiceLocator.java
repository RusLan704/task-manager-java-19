package ru.bakhtiyarov.tm.api.service;

import org.jetbrains.annotations.NotNull;

public interface ServiceLocator {

    @NotNull
    IUserService getUserService();

    @NotNull
    IAuthService getAuthService();

    @NotNull
    ICommandService getCommandService();

    @NotNull
    ITaskService getTaskService();

    @NotNull
    IProjectService getProjectService();

}
