package ru.bakhtiyarov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bakhtiyarov.tm.entity.Task;
import ru.bakhtiyarov.tm.entity.User;
import ru.bakhtiyarov.tm.enumeration.Role;

import java.util.Collection;
import java.util.List;

public interface IUserService {

    @NotNull
    List<User> findAll();

    void addAll(List<User> users);

    void clear();

    @Nullable
    User create(@Nullable String login,@Nullable String password);

    @Nullable
    User create(@Nullable String login,@Nullable String password,@Nullable String email);

    @Nullable
    User create(@Nullable String login,@Nullable String password, Role role);

    @Nullable
    User findById(@Nullable String id);

    @Nullable
    User findByLogin(@Nullable String login);

    @Nullable
    User removeByLogin(@Nullable String login);

    @Nullable
    User removeById(@Nullable String id);

    @Nullable
    User updatePassword(@Nullable String userId,@Nullable String password);

    @Nullable
    User updateUserEmail(@Nullable String userId,@Nullable String email);

    @Nullable
    User updateUserFirstName(@Nullable String userId,@Nullable String lastName);

    @Nullable
    User updateUserLastName(@Nullable String userId,@Nullable String LastName);

    @Nullable
    User updateUserMiddleName(@Nullable String userId, @Nullable String middleName);

    @Nullable
    User updateUserLogin(@Nullable String userId,@Nullable String login);

    @Nullable
    User lockUserByLogin(@Nullable String login);

    @Nullable
    User unLockUserByLogin(@Nullable String login);

}
