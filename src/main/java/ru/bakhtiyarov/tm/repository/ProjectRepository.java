package ru.bakhtiyarov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bakhtiyarov.tm.api.repository.IProjectRepository;
import ru.bakhtiyarov.tm.entity.Project;

import java.util.ArrayList;
import java.util.List;

public final class ProjectRepository implements IProjectRepository {

    @NotNull
    private final List<Project> projects = new ArrayList<>();

    @Override
    public void add(@NotNull final String userId, @NotNull final Project project) {
        project.setUserId(userId);
        projects.add(project);
    }

    @Override
    public void addAll(@NotNull List<Project> projects) {
        this.projects.addAll(projects);
    }

    @Override
    public void remove(@NotNull final String userId, @NotNull final Project project) {
        if (!userId.equals(project.getUserId())) return;
        projects.remove(project);
    }

    @NotNull
    @Override
    public List<Project> findAll(@NotNull final String userId) {
        @NotNull final List<Project> result = new ArrayList<>();
        for (@Nullable final Project project : projects) {
            if (project == null) continue;
            if (userId.equals(project.getUserId())) result.add(project);
        }
        return result;
    }

    @NotNull
    @Override
    public List<Project> findAll() {
        return projects;
    }

    @Override
    public void clear(@NotNull final String userId) {
        final List<Project> projects = findAll(userId);
        this.projects.removeAll(projects);
    }

    @Override
    public void clear() {
        projects.clear();
    }

    @Nullable
    @Override
    public Project findOneById(@NotNull final String userId, @NotNull final String id) {
        for (@Nullable final Project project : projects) {
            if (project == null) continue;
            if (userId.equals(project.getUserId())) continue;
            if (project.getId().equals(id)) return project;
        }
        return null;
    }

    @Nullable
    @Override
    public Project findOneByIndex(@NotNull final String userId, @NotNull final Integer index) {
        @NotNull final Project project = projects.get(index);
        if (userId.equals(project.getUserId())) return project;
        return null;
    }

    @Nullable
    @Override
    public Project findOneByName(@NotNull final String userId, @NotNull final String name) {
        for (@Nullable final Project project : projects) {
            if (project == null) continue;
            if (userId.equals(project.getUserId())) continue;
            if (project.getName().equals(name)) return project;
        }
        return null;
    }

    @Nullable
    @Override
    public Project removeOneByIndex(@NotNull final String userId, @NotNull final Integer index) {
        @Nullable final Project project = findOneByIndex(userId, index);
        if (project == null) return null;
        remove(userId, project);
        return project;
    }

    @Nullable
    @Override
    public Project removeOneById(@NotNull final String userId, @NotNull final String id) {
        @Nullable final Project project = findOneById(userId, id);
        if (project == null) return null;
        remove(userId, project);
        return project;
    }

    @Nullable
    @Override
    public Project removeOneByName(@NotNull final String userId, @NotNull final String name) {
        @Nullable final Project project = findOneByName(userId, name);
        if (project == null) return null;
        remove(userId, project);
        return project;
    }

}
