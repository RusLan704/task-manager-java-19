package ru.bakhtiyarov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bakhtiyarov.tm.api.repository.IUserRepository;
import ru.bakhtiyarov.tm.entity.User;

import java.util.ArrayList;
import java.util.List;

public final class UserRepository implements IUserRepository {

    @NotNull
    private final List<User> users = new ArrayList<>();

    @NotNull
    @Override
    public List<User> findAll() {
        return users;
    }

    @Override
    public void addAll(@NotNull List<User> users) {
        this.users.addAll(users);
    }

    @Override
    public void clear() {
        users.clear();
    }

    @Nullable
    @Override
    public User findById(@NotNull final String id) {
        for (@NotNull final User user : users) {
            if (id.equals(user.getId())) return user;
        }
        return null;
    }

    @Nullable
    @Override
    public User findByLogin(@NotNull final String login) {
        for (@NotNull final User user : users) {
            if (login.equals(user.getLogin())) return user;
        }
        return null;
    }

    @NotNull
    @Override
    public User add(@NotNull final User user) {
        users.add(user);
        return user;
    }

    @Nullable
    @Override
    public User removeByLogin(@NotNull final String login) {
        final @Nullable User user = findByLogin(login);
        if (user == null) return null;
        users.remove(user);
        return user;
    }

    @Nullable
    @Override
    public User removeById(@NotNull final String id) {
        final @Nullable User user = findById(id);
        if (user == null) return null;
        removeUser(user);
        return user;
    }

    @NotNull
    @Override
    public User removeUser(@NotNull final User user) {
        users.remove(user);
        return user;
    }

}
