package ru.bakhtiyarov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.bakhtiyarov.tm.command.AbstractCommand;
import ru.bakhtiyarov.tm.constant.TerminalConst;
import ru.bakhtiyarov.tm.entity.Project;

import java.util.List;

public final class ProjectListCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return TerminalConst.PROJECT_LIST;
    }

    @NotNull
    @Override
    public String description() {
        return "Show project list.";
    }

    @Override
    public void execute() {
        System.out.println("[LIST PROJECT]");
        @NotNull  final String userId = serviceLocator.getAuthService().getUserId();
        @NotNull  final List<Project> projects = serviceLocator.getProjectService().findAll(userId);
        for (@NotNull final Project project : projects) System.out.println(project);
        System.out.println("[OK]");
    }

}