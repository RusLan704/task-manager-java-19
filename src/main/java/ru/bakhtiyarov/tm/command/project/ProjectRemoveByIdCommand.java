package ru.bakhtiyarov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bakhtiyarov.tm.command.AbstractCommand;
import ru.bakhtiyarov.tm.constant.TerminalConst;
import ru.bakhtiyarov.tm.entity.Project;
import ru.bakhtiyarov.tm.util.TerminalUtil;

public final class ProjectRemoveByIdCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return TerminalConst.PROJECT_REMOVE_BY_ID;
    }

    @NotNull
    @Override
    public String description() {
        return "Remove project by id.";
    }

    @Override
    public void execute() {
        System.out.println("[SHOW PROJECT]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull  final String userId = serviceLocator.getAuthService().getUserId();
        @Nullable final Project project = serviceLocator.getProjectService().removeOneById(userId, id);
        if (project == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

}
