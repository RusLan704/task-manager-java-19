package ru.bakhtiyarov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bakhtiyarov.tm.command.AbstractCommand;
import ru.bakhtiyarov.tm.constant.TerminalConst;
import ru.bakhtiyarov.tm.entity.User;
import ru.bakhtiyarov.tm.util.TerminalUtil;

public final class UserUpdateMiddleNameCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return TerminalConst.USER_UPDATE_MIDDLE_NAME;
    }

    @NotNull
    @Override
    public String description() {
        return "Update user middle name.";
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE MIDDLE NAME]");
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        @Nullable final User user = serviceLocator.getUserService().findById(userId);
        if (user == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("ENTER MIDDLE NAME:");
        @NotNull final String middleName = TerminalUtil.nextLine();
        @Nullable final User userUpdated = serviceLocator.getUserService().updateUserMiddleName(userId, middleName);
        if (userUpdated == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[OK]");
    }

}