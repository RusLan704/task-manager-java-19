package ru.bakhtiyarov.tm.command.data.xml;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bakhtiyarov.tm.command.data.AbstractDataCommand;
import ru.bakhtiyarov.tm.dto.Domain;
import ru.bakhtiyarov.tm.enumeration.Role;
import ru.bakhtiyarov.tm.exception.data.DataLoadException;

import javax.imageio.IIOException;
import java.io.FileInputStream;

public final class DataXmlLoadCommand extends AbstractDataCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "data-xml-load";
    }

    @NotNull
    @Override
    public String description() {
        return "Load data from XML file.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA XML LOAD]");
        try (
                @NotNull final FileInputStream fileInputStream = new FileInputStream(FILE_XML)
        ) {
            @NotNull final ObjectMapper objectMapper = new XmlMapper();
            @NotNull final Domain domain = objectMapper.readValue(fileInputStream, Domain.class);
            setDomain(domain);
        } catch (IIOException e) {
            throw new DataLoadException(e);
        }
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}
