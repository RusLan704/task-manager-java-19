package ru.bakhtiyarov.tm.command.data.xml;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bakhtiyarov.tm.command.data.AbstractDataCommand;
import ru.bakhtiyarov.tm.enumeration.Role;

import java.io.File;
import java.nio.file.Files;

public final class DataXmlClearCommand extends AbstractDataCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "data-xml-clear";
    }

    @NotNull
    @Override
    public String description() {
        return "Clear xml file.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA XML CLEAR]");
        final File file = new File(FILE_XML);
        Files.deleteIfExists(file.toPath());
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}