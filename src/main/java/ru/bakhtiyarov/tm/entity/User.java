package ru.bakhtiyarov.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bakhtiyarov.tm.enumeration.Role;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
public class User extends AbstractEntity implements Serializable {

    @NotNull
    private String login = "";

    @NotNull
    private String passwordHash = "";

    @NotNull
    private String email = "";

    @NotNull
    private String firstName = "";

    @NotNull
    private String lastName = "";

    @NotNull
    private String middleName = "";

    @NotNull
    private Role role = Role.USER;

    private Boolean locked = false;

}