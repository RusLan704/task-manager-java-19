package ru.bakhtiyarov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bakhtiyarov.tm.api.repository.IUserRepository;
import ru.bakhtiyarov.tm.api.service.IUserService;
import ru.bakhtiyarov.tm.entity.User;
import ru.bakhtiyarov.tm.enumeration.Role;
import ru.bakhtiyarov.tm.exception.empty.*;
import ru.bakhtiyarov.tm.util.HashUtil;

import java.util.List;

public class UserService implements IUserService {

    @NotNull
    private final IUserRepository userRepository;

    public UserService(@NotNull final IUserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public void addAll(List<User> users) {
        userRepository.addAll(users);
    }

    @Override
    public void clear() {
        userRepository.clear();
    }

    @NotNull
    @Override
    public List<User> findAll() {
        return userRepository.findAll();
    }

    @Nullable
    @Override
    public User findById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return userRepository.findById(id);
    }

    @Nullable
    @Override
    public User findByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        return userRepository.findByLogin(login);
    }

    @Nullable
    @Override
    public User removeByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        return userRepository.removeByLogin(login);
    }

    @Nullable
    @Override
    public User removeById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return userRepository.removeById(id);
    }

    @Nullable
    @Override
    public User create(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        @Nullable final User user = new User();
        user.setLogin(login);
        user.setRole(Role.USER);
        user.setPasswordHash(HashUtil.salt(password));
        return userRepository.add(user);
    }

    @Nullable
    @Override
    public User create(@Nullable final String login, @Nullable final String password, @Nullable final String email) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        @Nullable final User user = create(login, password);
        if (user == null) return null;
        user.setEmail(email);
        return user;
    }

    @Nullable
    @Override
    public User create(@Nullable final String login, @Nullable final String password, @Nullable final Role role) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (role == null) throw new EmptyRoleException();
        @Nullable final User user = create(login, password);
        if (user == null) return null;
        user.setRole(role);
        return user;
    }

    @Nullable
    @Override
    public User updatePassword(@Nullable final String userId, @Nullable final String password) {
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        @Nullable final User user = findById(userId);
        if (user == null) return null;
        user.setPasswordHash(HashUtil.salt(password));
        return user;
    }

    @Nullable
    @Override
    public User updateUserEmail(@Nullable final String userId, @Nullable final String email) {
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        @Nullable final User user = findById(userId);
        if (user == null) return null;
        user.setEmail(email);
        return user;
    }

    @Nullable
    @Override
    public User updateUserFirstName(@Nullable final String userId, @Nullable final String firstName) {
        if (firstName == null || firstName.isEmpty()) throw new EmptyFirstNameException();
        @Nullable final User user = findById(userId);
        if (user == null) return null;
        user.setFirstName(firstName);
        return user;
    }

    @Nullable
    @Override
    public User updateUserLastName(@Nullable final String userId, @Nullable final String lastName) {
        if (lastName == null || lastName.isEmpty()) throw new EmptyLastNameException();
        @Nullable final User user = findById(userId);
        if (user == null) return null;
        user.setLastName(lastName);
        return user;
    }

    @Nullable
    @Override
    public User updateUserMiddleName(@Nullable final String userId, @Nullable final String middleName) {
        if (middleName == null || middleName.isEmpty()) throw new EmptyMiddleNameException();
        @Nullable final User user = findById(userId);
        if (user == null) return null;
        user.setMiddleName(middleName);
        return user;
    }

    @Nullable
    @Override
    public User updateUserLogin(@Nullable final String userId, @Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @Nullable final User user = findById(userId);
        if (user == null) return null;
        user.setLogin(login);
        return user;
    }

    @Nullable
    @Override
    public User lockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @Nullable final User user = findByLogin(login);
        if (user == null) return null;
        user.setLocked(true);
        return user;
    }

    @Nullable
    @Override
    public User unLockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @Nullable final User user = findByLogin(login);
        if (user == null) return null;
        user.setLocked(false);
        return user;
    }

}